function reset(){
  localStorage.removeItem('score');
}

function playGame(playMove){

  let score = JSON.parse((localStorage.getItem('score')))
  || {
    wins : 0,
    losses : 0,
    ties: 0
  };

  // if(!score){
  //   score = {
  //     wins : 0,
  //     losses : 0,
  //     ties: 0
  //   }
  // }

  let computerMove = pickComputerMove();

  let result = '';

  if (playMove === 'rock'){
    if(computerMove === 'rock'){
      result = 'Ties';
    }
    else if(computerMove === 'paper'){
      result = 'You Lose';
    }
    else if(computerMove === 'scissors'){
      result = 'You Win';
    }
  }

  else if (playMove === 'paper'){
    if(computerMove === 'rock'){
      result = 'You Win';
    }
    else if(computerMove === 'paper'){
      result = 'Ties';
    }
    else if(computerMove === 'scissors'){
      result = 'You Lose';
    }
  }

  else if (playMove === 'scissors'){
    if(computerMove === 'rock'){
      result = 'You Lose';
    }
    else if(computerMove === 'paper'){
      result = 'You Win';
    }
    else if(computerMove === 'scissors'){
      result = 'Ties';
    }
  }

  if(result === 'You Win'){
    score.wins += 1;
  }else if (result === 'You Lose'){
    score.losses += 1;
  }
  else if (result === 'Ties'){
    score.ties += 1;
  }

 
  localStorage.setItem('score', JSON.stringify(score));

  alert(`You picked ${playMove} and computer picked ${computerMove} so, ${result}
  wins: ${score.wins} , losses: ${score.losses}, tie: ${score.ties}`);
}


function pickComputerMove(){
  let computer = Math.random();

  let computerMove = '';

  if(computer  > 0 && computer< 1/4){
    computerMove = 'rock';
  }
  else if(computer  > 1/4 && computer < 1/2){
    computerMove = 'paper';
  }
  else if (computer > 1/2){
    computerMove = 'scissors';
  }

  return computerMove;
}




// Distructuring a Reference (object)
const object1 = {
  message: 'good job',
  price: 799
}
//  const message = object1.message;
// // if both variable and object attribute have same name, 
////////////////////////////////
// distruscturing 
 const {message, price} = object1;
 console.log(message, price);

//  Shortnabd propery 
const object2 = {
  message,
  method(){
    console.log('hello forks')
  }
};

console.log(object2);
object2.method();